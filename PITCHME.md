---
@snap[west headline]
## Spring Framework Recap
@snapend

@snap[south-west byline]
What have we done until now?
@snapend
---
### Today:
  1. Dry SoCs
  1. Spring Bean
  1. Spring Application Context
  1. Interacting with Application Context
  1. Spring's Proxy
  1. Handeling Properties
  1. Profiles
---
### First: Spring is   ?
---
### Dry SoCs?
--- 
### Dry: Do not repeat yourself
--- 
### SoCs: Separation of concerns
--- 
### What is a Spring Bean?
---
### Spring's Application Context?
 - To keep track of Spring beans, Spring uses an **Application Context**.
---
### Interacting with the Spring's Application Context?
 - Add / Retrieve items. 
 - For later: Bonus Point - What about delete?
---
### How do we add objects to the Application Context?
  - @Configuration + @Bean 
  - @Component
---
### @Configuration + @Bean
```
@Configuration
class ApplicationConfiguration {
  
  @Bean
  public ApplicationService applicationService() {
    return new ApplicationService();
  }
  
}
```
---
### @Component
```
@Component
class ApplicationService {
  // .. 
}
```
---
### @Component
  - @Service
  - @Repository
  - @Controller
  - @RestController
---
### Splitup Configuration Files?
- @Import
```
@Configuration
@Import({MoreConfiguration.class})
public class ExampleConfiguration { ...
```
---
### How do we Retrieve Objects from the Application Context?
  - @Autowired
  - What can we annotate with @Autowire? 
  - Best Practice?
---
### Spring's Proxy!!?
---
### Normally 
Let say two people wish to send each other a message

![Normal](/img/normal.png)
---
### With Proxy
![Proxy](/img/proxy.png)
---
### In Practice:
![Our Application](/img/ourapp.png)
---
### Spring Bean scope?
- @Scope?
- Bonus Point: What about delete?
---
### Spring Bean Names
- @Bean
```
@Bean(name="myBeanName")
```
- Annotation Based Annotations
```
@Component("myBeanName")
```
---
### @Qualifier
```
@Autowired
@Qualifier("myBeanName")
private MyBean myBean;
```
---
### @Primary
```
@Bean
@Primary
MyBean bean() {...}
```
---
### Loading and reading properties?
---
### @Value
```
@Value("${name}")
private String stringValue;
```
- In application.properties
```
name=george
```

---
### @Configuratation Properties
```
@ConfigurationProperties(prefix = "person")
public class PersonProperties {
     
    private String name;
    private int age;
    // getters and setters!!!
}
```

- In application.properties
```
person.name=Roger
person.age=105
```
---

### @Profile

- Example:
```
@Component
@Profile("dev")
public class DevDatasourceConfig
```